package geneticDriver;

/* ************************************
Jonathan W. Lartigue
Auburn University
Masters Project
Spring 2002
(C) Copyright 2001-2002 by Jonathan W. Lartigue
All rights reserved.
Modification by
Douglas Rogers
Kennesaw State University
Software Engineering
Independent Study 2014-2015
By permission
************************************ */
import java.util.*;
import java.io.*;
import java.text.DecimalFormat;
import java.io.FileWriter;
import java.io.IOException;


public class GeneticDriver2 {
    // static int report = 3;
    static Random rnd = new Random();
    static DecimalFormat fmt = new DecimalFormat("0.000");
    static double[] weights;
    static double[] values;
    static double sumOfAllWeights;
    static ArrayList<SimpleKnapsack> sacks = null;
    static int itemsInStore_DNALength;
    static int opsToFindBest;
    static int childrenAdded;
    static boolean cheat;
    static int currentPopulationSize;
    static double knapsackCapacity;
    static String output;
    static double cheatIncrement;
    static boolean GA, GAVI, GAVVI;

    // virus variables
    static Virus infectiveVirus; // the most infective virus that will transfer genes on each mating
    static boolean variableVirus = false;
    static int virusSize = 3;
    static Virus[] viruses = new Virus[30]; // virus population

    // knapsack variables
    static final int MAX_GENERATIONS = 50000;
    static int trials = 50;
    // parameters to set
    static double[] cheatIncrements = new double[]{ 1, 2, 3 };
    private static final String[] gaTypes = new String[]{  "GA" };// { "GA", "GAVI", "GAVVI" }
    private static final int[] populationSets = new int[]{ 50 };// { 50, 100, 200 }
    private static final String[] dnaDataStores = new String[]{ "ksp100.prb" };// { "ksp50.prb", "ksp100.prb", "ksp200.prb" }
    private static final double[] capacities = new double[]{ .25 }; // maybe later add { .10, .25, .50, .75 }
    private static final boolean[] cheatLoopToRun = new boolean[] {false,true}; // {true, false}


    static String fileNameToWrite = "123cheaters only_output_9am_05-25-15.csv";

    public static void main(String[] args) {

        try {
            FileWriter writer = new FileWriter(fileNameToWrite, true);

            int avgOpsToFindBest, avgChildrenAdded;
            double maxFitness;
            final long startTime = System.currentTimeMillis();

            for (double cheatIncrementToTest: cheatIncrements){
                cheatIncrement = cheatIncrementToTest;

                for (String algorithmToRun: gaTypes) {
                    if (algorithmToRun.equals("GA")) {
                        GA = true;
                        GAVI = false;
                        GAVVI = false;
                    } else if (algorithmToRun.equals("GAVI")) {
                        GA = false;
                        GAVI = true;
                        GAVVI = false;
                    } else {
                        GA = false;
                        GAVI = false;
                        GAVVI = true;
                    }

                    for (int currentPopulation : populationSets) {
                        currentPopulationSize = currentPopulation;
                        output = "Population of knapsacks = ,,,," + currentPopulationSize;
                        // System.out.println(output);
                        writer.write(output + "\r\n");

                        for (String DnaSet : dnaDataStores) {
                            loadWeightsAndValues(DnaSet);
                            output = "Total Store Capacity = ,,,," + fmt.format(sumOfAllWeights);
                            // System.out.println(output);
                            writer.write(output + "\r\n");

                            for (double capacity : capacities) {
                                knapsackCapacity = sumOfAllWeights * capacity;
                                output = "\tCurrent knapsack capacity = ,,,," + fmt.format(knapsackCapacity);
                                // System.out.println(output);
                                writer.write(output + "\r\n");

                                for (boolean currentCheatOption : cheatLoopToRun) {
                                    cheat = currentCheatOption;
                                    output = "\trun ,cheatIncrement = ," + cheatIncrement + ",,,," + algorithmToRun + ", c: " + cheat;
                                    //System.out.println(output);
                                    writer.write(output + "\r\n");

                                    // reset counters
                                    avgOpsToFindBest = 0;
                                    avgChildrenAdded = 0;
                                    maxFitness = 0;

                                    for (int i = 0; i < trials; i++) {

                                        generateInitialSackPopulation();
                                        geneticSearch();
                                        output = "\t\t\tTrial: ," + i + ",: Children added: ," + childrenAdded + ",\tOps to best= ," + opsToFindBest +
                                                ",\tthe max fitness is: ," + fmt.format(sacks.get(currentPopulationSize - 1).getFitness()); //
                                        // System.out.println(output);
                                        writer.write(output + "\r\n");
                                        avgOpsToFindBest += opsToFindBest;
                                        avgChildrenAdded += childrenAdded;
                                        if (sacks.get(currentPopulationSize - 1).getFitness() > maxFitness)
                                            maxFitness = sacks.get(currentPopulationSize - 1).getFitness();
                                    }
                                    output = ",,Avg # added: ," + avgChildrenAdded / trials + ",Result: avg ops  = ," + avgOpsToFindBest / trials +
                                            ",\tmax fitness found is: ," + fmt.format(maxFitness);//
                                    // System.out.println(output);
                                    writer.write(output + "\r\n");
                                }
                            }
                        }
                    }
                }
            }


            final long endTime = System.currentTimeMillis();
            output = "Total execution time: " + (endTime - startTime);
            System.out.println(output);
            writer.write(output);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.flush();
    }



    public static void geneticSearch()  {
        // reset counters
        opsToFindBest = 0;
        childrenAdded = 0;
        SimpleKnapsack child;

        if (GAVVI || GAVI) createVirusPopulation();

        for (int generation = 0; generation < MAX_GENERATIONS; generation++)
        {
            /*
            the function to give parents a bonus for producing good children is working, and it shows as an arc:
            the bonus grows up to a point: once better children can no longer be found, the overall bonus starts dropping.

            if (generation == MAX_GENERATIONS-2){
                int i = 0;
                for (SimpleKnapsack individual :sacks) {
                    System.out.println("Sack # " + i++ + " " + individual.getParentFitness());
                }
            }
            */

            // Step 1: Select two parents, each with random tournament selection.
            ArrayList<Integer> parents = tournamentSelection();

            // Step 2: Mate the parents and produce a child.
            if (GA) {
                child = (SimpleKnapsack) sacks.get(parents.get(0)).mate(sacks.get(parents.get(1)));
            } else {
                promoteNewInfectiveVirus();
                child = (SimpleKnapsack) sacks.get(parents.get(0)).viralMate( sacks.get(parents.get(1)), infectiveVirus);
            }

            // Step 3: Infant mortality - if child is not more fit than least fit individual
            //         in currentPopulationSize, it dies.
            if ( child.getFitness() > sacks.get(0).getFitness()) {
                // child is more fit than the weakest in the pool, so add to pool.
                double best = sacks.get(currentPopulationSize - 1).getFitness();
                if (child.getFitness() > best)
                    opsToFindBest = generation;

                // give child average of parent fitness (include pending increase)
                double fitnessToAdd = (sacks.get(parents.get(0)).getParentFitness() + sacks.get(parents.get(1)).getParentFitness())/2;
                /* debug : this fitness to add works:
                System.out.println(fitnessToAdd);
                */
                child.parentFitness = fitnessToAdd;

                sacks.remove(0);
                sacks.add(child);
                Collections.sort(sacks);
                childrenAdded++;

                // increase the parentFitness
                sacks.get(parents.get(0)).increaseParentFitness();
                sacks.get(parents.get(1)).increaseParentFitness();


            } else {
                // decrease parentFitness
                sacks.get(parents.get(0)).decreaseParentFitness();
                sacks.get(parents.get(1)).decreaseParentFitness();
            }
            /*// Print out a status update every so many generations.
            if (report < 3 && (((generation % 20000) == 19999 || generation == 0)))
            System.out.println("At generation " + generation + "\tOps to best= " + opsToFindBest + ": Children added: " + childrenAdded +
                    "\tthe max fitness is: " + fmt.format(sacks.get(currentPopulationSize-1).getFitness()));

            //* Debug:// Print out a few sacks every 25000 generations.
            if (report < 3 && (generation % 25000 == 24999 || generation == 0)){
            System.out.println(sacks.get(0).toString());
            System.out.println(sacks.get((int) (currentPopulationSize * .25)).toString());
            System.out.println(sacks.get((int) (currentPopulationSize*.50)).toString());
            System.out.println(sacks.get((int) (currentPopulationSize*.75)).toString());
            System.out.println(sacks.get(currentPopulationSize-1).toString());
            }
            //*/
        }
    }
    /*
    if (GAVVI) {
                // Find the maximum infective virus in the population and set infectiveVirus to it
                Virus infectiveVirus = pickTopVirus(viruses, getPercentOfPopulation);
                // Mate parents and infect child GAVVI (vertical viral infection)
                child = (SimpleKnapsack) knapsackPopulation[parents[0]].viralMate(knapsackPopulation[parents[1]], infectiveVirus);
            } else {
                // Mate the two and get a child
                child = (SimpleKnapsack) knapsackPopulation[parents[0]].mate(knapsackPopulation[parents[1]]);
            }
     */

    public static void generateInitialSackPopulation(){
        sacks = new ArrayList<SimpleKnapsack>();
        sacks.clear();

        for (int i = 0; i < currentPopulationSize; i++) {
            SimpleKnapsack sack = new SimpleKnapsack(itemsInStore_DNALength, knapsackCapacity, weights, values);
            sacks.add(sack);
            //DEBUG: System.out.println(sack);
        }
        Collections.sort(sacks);
        // DEBUG = only print at intitialization of trial
        // System.out.println("The initial sack count is: " + sacks.size() + " and total capacity is: " + fmt.format(knapsackCapacity));
    }

    private static ArrayList<Integer> tournamentSelection() {
        // Find two mates via k=2 tournament selection
        ArrayList<Integer> parent = new ArrayList<Integer>();
        ArrayList<Integer> potentialParents = randomSelection();

        // ArrayList<SimpleKnapsack> best = new ArrayList<SimpleKnapsack>();
        double fitnessP0 = sacks.get(potentialParents.get(0)).getFitness();
        double fitnessP1 = sacks.get(potentialParents.get(1)).getFitness();
        double fitnessP2 = sacks.get(potentialParents.get(2)).getFitness();
        double fitnessP3 = sacks.get(potentialParents.get(3)).getFitness();

        if (cheat) {

            // remove if statements = all parents have chance for advantage if they create better children
            if (sacks.get(potentialParents.get(0)).cheater)
            fitnessP0 += sacks.get(potentialParents.get(0)).getFitness() + sacks.get(potentialParents.get(0)).getParentFitness();
            if (sacks.get(potentialParents.get(1)).cheater)
            fitnessP1 += sacks.get(potentialParents.get(1)).getFitness() + sacks.get(potentialParents.get(1)).getParentFitness();
            if (sacks.get(potentialParents.get(2)).cheater)
            fitnessP2 += sacks.get(potentialParents.get(2)).getFitness() + sacks.get(potentialParents.get(2)).getParentFitness();
            if (sacks.get(potentialParents.get(3)).cheater)
            fitnessP3 += sacks.get(potentialParents.get(3)).getFitness() + sacks.get(potentialParents.get(3)).getParentFitness();

        }
        parent.add( ( fitnessP0 > fitnessP1 ) ? potentialParents.get(0) : potentialParents.get(1) );
        parent.add( ( fitnessP2 > fitnessP3 ) ? potentialParents.get(2) : potentialParents.get(3));

        return parent;
    }

    private static void createVirusPopulation(){

        // Create initial population of viruses
        for (int i = 0; i < viruses.length; i++) {
            if (variableVirus){
                // debug // System.out.print("s = " + virusSize + " sackSize = " + fieldSize);
                viruses[i] = new VariableSizeBinaryVirus(virusSize, itemsInStore_DNALength);
            } else {
                viruses[i] = new SimpleBinaryVirus(virusSize, itemsInStore_DNALength);
            }
        }
    }

    public static void promoteNewInfectiveVirus(){
        // Find the maximum infective virus in the population and set infectiveVirus to it among randomly selected viruses
        int vMax = 0;
        int pick;
        int vMaxValue = -Integer.MAX_VALUE;
        for (int i = 0; i < viruses.length/4; i++) {
            pick = rnd.nextInt(viruses.length);
            int currentInfectionRate = viruses[pick].getIR();
            if (currentInfectionRate > vMaxValue ) {
                vMax = pick;
                vMaxValue = currentInfectionRate;
            }
        }
        infectiveVirus = viruses[vMax];
    }
    /*
    private static void chanceForCheatersToWin(ArrayList<Integer> parent, ArrayList<Integer> potentialParents) {
        // select parents generally with better fitness
        // chance that cheating makes a difference


        Double chance = .10;

        if (rnd.nextDouble() < chance) {
            // reward cheater
            if (sacks.get(potentialParents.get(0)).cheater && !sacks.get(potentialParents.get(1)).cheater)
                parent.add(potentialParents.get(0));
            if (!sacks.get(potentialParents.get(0)).cheater && sacks.get(potentialParents.get(1)).cheater)
                parent.add(potentialParents.get(1));
                // if both are truthful or both cheating, just get better fitness
            else
                parent.add((sacks.get(potentialParents.get(0)).getFitness() > sacks.get(potentialParents.get(1)).getFitness())
                        ? potentialParents.get(0) : potentialParents.get(1));
        } else {
            // cheating didn't work, use better fitness
            parent.add((sacks.get(potentialParents.get(0)).getFitness() > sacks.get(potentialParents.get(1)).getFitness())
                    ? potentialParents.get(0) : potentialParents.get(1));
        }

        // repeat for next contenders
        if (rnd.nextDouble() < chance) {
            // reward cheater
            if (sacks.get(potentialParents.get(2)).cheater && !sacks.get(potentialParents.get(3)).cheater)
                parent.add(potentialParents.get(2));
            if (!sacks.get(potentialParents.get(2)).cheater && sacks.get(potentialParents.get(3)).cheater)
                parent.add(potentialParents.get(3));
                // if both are truthful or both cheating, just get better fitness
            else
                parent.add((sacks.get(potentialParents.get(2)).getFitness() > sacks.get(potentialParents.get(3)).getFitness())
                        ? potentialParents.get(2) : potentialParents.get(3));
        } else {
            // cheating didn't work, use better fitness
            parent.add((sacks.get(potentialParents.get(2)).getFitness() > sacks.get(potentialParents.get(3)).getFitness())
                    ? potentialParents.get(2) : potentialParents.get(3));
        }
    }
    */

    private static ArrayList<Integer> randomSelection() {
        // select 4 unique genomes at random
        // last two picks are unused if the selection process is purely random
        ArrayList<Integer> parents= new ArrayList<Integer>();
        int pick;
        do {
            pick = rnd.nextInt(currentPopulationSize);
            if (! parents.contains(rnd.nextInt(currentPopulationSize)))
                parents.add(pick);
        } while (parents.size() < Math.max(4, currentPopulationSize*.08));
        // System.out.println("Total parents selected at random = " + parents.size());
        return parents;
    }

    public static void loadWeightsAndValues(String filename) {
        StringTokenizer tokenizer;
        FileReader theFile;
        BufferedReader fileIn;
        String line;
        sumOfAllWeights = 0.0;

        try {
            theFile = new FileReader(filename);

            fileIn = new BufferedReader(theFile);
            String firstLine = fileIn.readLine();
            tokenizer = new StringTokenizer(firstLine);
            itemsInStore_DNALength = Integer.parseInt(tokenizer.nextToken());
            weights = new double[itemsInStore_DNALength];
            values  = new double[itemsInStore_DNALength];
            // Debug: System.out.println("Items in store: " + itemsInStore_DNALength);
            for (int i= 0; i < itemsInStore_DNALength; i++)
            {
                line = fileIn.readLine();
                tokenizer = new StringTokenizer(line);
                values[i]  = Double.parseDouble(tokenizer.nextToken());
                weights[i] = Double.parseDouble(tokenizer.nextToken());
                sumOfAllWeights+=weights[i];
                // Debug: System.out.println("Item " + i + ": " + weights[i] + ", $" + values[i]);
            }
        }
        catch (Exception e)
        {
            // crash gloriously
            System.out.println("ERROR: " + e);
        }
    }
}