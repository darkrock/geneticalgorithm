package geneticDriver;

/* ************************************
Jonathan W. Lartigue
Auburn University
Masters Project
Spring 2002
(C) Copyright 2001-2002 by Jonathan W. Lartigue
All rights reserved.
File: Knapsack.java
************************************ */
public interface Knapsack extends Comparable{
	
   public double getCapacity();
   public double getWeight();
   public double getValue();
   public boolean getObject(int position);
   public double fitnessFn();
   public double getFitness();
   public Knapsack mate(Knapsack obj);
   public int compareTo(Object other);
	
}
