package geneticDriver;

/* ************************************
Jonathan W. Lartigue
Auburn University
Masters Project
Spring 2002

(C) Copyright 2001-2002 by Jonathan W. Lartigue
All rights reserved.

File: SimpleBinaryVirus.java
************************************ */

import java.util.Random;

public class SimpleBinaryVirus implements Virus {
	/* from Virus: 
	public int getSize();
	public int getLocus();
	public boolean[] getGenes();
	public int getIR();
	public void incIR();
	public void decIR();
	*/
	// protected int size; // Size of the virus (in genes)
	protected int locus; // Locus at which the virus will insert its genes
	protected boolean[] genes; // array of genes
	protected int infectionRate; // rate that this virus infects and produces a better child
	protected static Random rnd = new Random(); // random generator
	
	public SimpleBinaryVirus(int s, int sackSize) {
		genes = new boolean[s];
		locus = rnd.nextInt(sackSize - s);
		for (int i = 0; i < s; i++)	{
			genes[i] = rnd.nextBoolean();
		}
	}
	
	public int getSize() {
		return genes.length;
	}
	
	public int getLocus() {
		return locus;
	}
	
	public boolean[] getGenes() {
		return genes;
	}
	
	public int getIR() {
		return infectionRate;
	}
	
	public void incIR() {
		infectionRate++;
	}
	
	public void decIR() {
		infectionRate--;
	}
	
	public String toString() {
		String temp = "";
		for (int i = 0; i < getSize(); i++) {
			temp = temp + (genes[i] ? 1 : 0) + " ";
		}
		return temp;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			SimpleBinaryVirus v = new SimpleBinaryVirus(i, 10);
			System.out.println(v);
		}
	}
}
