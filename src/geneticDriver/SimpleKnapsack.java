package geneticDriver;

/* ************************************
Jonathan W. Lartigue
Auburn University
Masters Project
Spring 2002

(C) Copyright 2001-2002 by Jonathan W. Lartigue
All rights reserved.

File: SimpleKnapsack.java
************************************ */

import java.util.Random;
import java.text.DecimalFormat;
// import java.lang.reflect.*;

public class SimpleKnapsack implements Knapsack {
    DecimalFormat fmt = new DecimalFormat("0.0000");
    private int sackSize;
    private boolean contents[];
    private double[] weights;
    private double[] values;
    private double capacity;
    private double fitness;
    public double parentFitness = 0;
    boolean cheater;

    // variables to set
    private double maxParentFitnessIncrease = .05;
    private final double MUTATION_FACTOR = 0.1;
    private double percentDefector = .10;

    /*
    Game theory models as follows:
    0 = (PD) prisoner's dilemma
    1 = (CG) chicken game
    2 = (MP) mixed polymorphism
    3 = (FOF) friend or foe
    4 = (FD) facultative defection
    5 = (BS) battle of sexes
    6 = (SH) stag hunt
    7 = none ??? may not need, just overload fitnessFn function for game theory

    private int gameTheoryModel = 0;
    private final double[][] matrix = new double[][]{
    { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 },
    { 0.4, 0.5, 0.5, 1.0, 0.3, 1.0, 0.7 },
    { 1.0, 9.0, 0.5, 0.5, 0.3,-1.0,-0.2 },
    { 0.2, 0.17, 1.0, 1.0, 0.0, 0.3, 1.0 }
    };
    private double k = matrix[0][gameTheoryModel];
    private double s1 = matrix[1][gameTheoryModel];
    private double s2 = matrix[2][gameTheoryModel];
    private double c = matrix[3][gameTheoryModel];

    private double rewardCC = k;
    private double suckerCD = k-s1;
    private double temptationDC = k+s2;
    private double punishDD = k-c;
    private double maxPayoff = java.lang.Math.max(java.lang.Math.max(rewardCC, suckerCD),
    java.lang.Math.max(temptationDC, punishDD));
    */

    public double getFitness() {
        return fitness;
    }

    Random genRandom = new Random();

    public void increaseParentFitness() {
        // if statement creates ceiling
        //if (parentFitness < maxParentFitnessIncrease)
        parentFitness += GeneticDriver2.cheatIncrement;
    }

    public void decreaseParentFitness() {
        // if statement creates floor
        // if (parentFitness > .00)
        parentFitness -= GeneticDriver2.cheatIncrement;
    }

    public double getParentFitness() {
        return parentFitness;
    }

    public SimpleKnapsack(int size, double cap, double[] newweights, double[] newvalues) {
        // Create new knapsack and randomize its contents
        capacity = cap;
        sackSize = size;
        weights = newweights;
        values = newvalues;
        // if next random is less than defector percentage, we have a cheater on our hands.
        cheater = (genRandom.nextFloat() < percentDefector);
        contents = new boolean[size];

        for (int i = 0; i < sackSize; i++) {
            contents[i] = genRandom.nextBoolean();
        }

        fitness = fitnessFn();
    }

    // used by various functions to create a specific knapsack
    public SimpleKnapsack(boolean[] initialContentsArray, double sackCapacity, double[] initialWeights, double[] initialValues) {
        capacity = sackCapacity;
        weights = initialWeights;
        values = initialValues;
        sackSize = initialContentsArray.length;
        contents = initialContentsArray;
        fitness = fitnessFn();

        // if next random is less than defector percentage, we have a cheater on our hands.
        cheater = (genRandom.nextFloat() < percentDefector);
    }

    public double getCapacity() {
        return capacity;
    }

    public double getWeight() {
        double sumOfWeights = 0;
        for (int i = 0; i < sackSize; i++) {
            if (contents[i]) {
                sumOfWeights += weights[i];
            }
        }
        return sumOfWeights;
    }

    public double getValue() {
        double sumOfValues = 0;
        for (int i = 0; i < sackSize; i++) {
            if (contents[i]) {
                sumOfValues += values[i];
            }
        }
        return sumOfValues;
    }

    public boolean getObject(int position) {
        return contents[position];
    }


    public void setGeneAt(int position, boolean gene) {
        if (position < contents.length) {
            contents[position] = gene;
        }
    }

    // fitnessFn function
    public double fitnessFn() {
        double fx;
        if (getCapacity() - getWeight() < 0) {
            // THE SACK IS OVER WEIGHT
            fx = getCapacity() - getWeight();
        } else {
            // THE SACK IS WITHIN THE WEIGHT CONSTRAINTS
            fx = getValue();//  + getWeight();
        }

        return fx;
    }

    /*// overloaded fitnessFn function includes deception
    public double fitness(boolean currentOpponent) {
    // new function operates with normalized values
    // default fx is formula for cheater, since most of currentPopulationSize is cooperating.
    double fx = getFxForCooperator();
    double gameTheory;
    // determine gameTheory factor
    if (cheater) {
    if (currentOpponent) {
    // opponent is cheater
    gameTheory = rewardCC / maxPayoff;
    }
    else {
    // opponent is defector
    gameTheory = suckerCD / maxPayoff;
    }
    }
    else {
    // revise fx for this defector
    fx = getFxForDefector();
    if (currentOpponent) {
    // opponent is cheater
    gameTheory = temptationDC / maxPayoff;
    }
    else {
    // opponent is defector
    gameTheory = punishDD / maxPayoff;
    }
    }
    // add game theory factor
    fx += gameTheory;
    return fx;
    }

    private double getFxForCooperator() {
    double fx;
    if ( ! overweight()  ) {
    fx = getValue()/getMaxFitness();
    }
    else {
    fx = (getCapacity() - getWeight())/getMaxFitness();
    }
    if  (fx == 0) System.err.println("Zero value for getFxForCooperator()");
    return fx;
    }

    private double getFxForDefector() {
    double fx;
    if ( ! overweight()  ) {
    fx = getValue() + getCheatingDegree()/getMaxFitness();
    }
    else {
    fx = (getCapacity() - (getWeight() - getCheatingDegree()))/getMaxFitness();
    }
    return fx;
    }

    private boolean overweight() {
    // determine if sack is overweight
    return getCapacity() - getWeight() < 0;
    }
    public SimpleKnapsack toggleBit(int bit) {
    boolean[] childcontents = new boolean[sackSize];

    System.arraycopy(contents,0,childcontents,0,sackSize);
    childcontents[bit] = ! childcontents[bit];
    return new SimpleKnapsack(childcontents, capacity, weights, values);
    }
    */

    public Knapsack viralMate(Knapsack obj, Virus v) {

        boolean[] childContents = this.mateRandomCrossover(obj);

        SimpleKnapsack child = new SimpleKnapsack(childContents, capacity, weights, values);

        if (GeneticDriver2.GAVVI) {
            child.infect(v);
        }

        return child;
    }

    public Knapsack mate(Knapsack obj) {

        boolean[] childContents = this.mateRandomCrossover(obj);

        return new SimpleKnapsack(childContents, capacity, weights, values);
    }

    public boolean[] mateRandomCrossover(Knapsack obj) {
//        this method is necessary instead of a specific crossover or even a determined crossover.
        boolean[] childContents = new boolean[sackSize];
        boolean newBit;

        for (int i = 0; i < sackSize; i++) {
            // randomly pick a bit from the two mates
            newBit = genRandom.nextBoolean() ? this.getObject(i) : obj.getObject(i);
            if (genRandom.nextDouble() <= MUTATION_FACTOR) {
                // Randomly mutate the bit by toggling it
                newBit = (!newBit);
            }
            childContents[i] = newBit;
        }
        return childContents;
    }

    public SimpleKnapsack infect(Virus infectingVirus) {

        // This infects regardless of improvement!
        double previousFitness = this.fitness;

        // Perform viral mutation
        // the locus is the point where the virus will transfer its genes
        int locus = infectingVirus.getLocus();
        boolean[] genes = infectingVirus.getGenes();
        boolean[] previous = new boolean[genes.length];

        // spread Infection child genes
        for (int i = 0; i < genes.length; i++) {
            previous[i] = this.getObject(locus + i);
            this.setGeneAt(locus + i, genes[i]);
        }


        double finalFitness = fitnessFn();

        if (finalFitness > previousFitness) {
            // if mutation increased fitnessFn increase the viruses infection rate
            infectingVirus.incIR();
            // also extend virus's the genotype
            // if it is an adjustible size virus
            if (GeneticDriver2.variableVirus) {
                // Call the virus's induction function
                // and pass it the array of te knapsack
                // contents so it can strip a gene
                // System.out.println("*** DEBUG *** Calling virus induction routine");
                // System.out.println("*** DEBUG *** Virus before induction: " + v);
                ((VariableSizeBinaryVirus) infectingVirus).induction(contents);
                // System.out.println("*** DEBUG *** Virus after induction: " + v);
            }
        } else {
            // decrease the virus's infection rate
            infectingVirus.decIR();
            // and do not infect.
            for (int i = 0; i < genes.length; i++) {
                this.setGeneAt(locus + i, previous[i]);
            }
        }
        return this;
    }

    public String toString() {
        String str = "";
        str = str + "Sack Fitness:" + fmt.format(fitness) + " cheater:" + cheater;// + "\tCapacity=" + fmt.format(getCapacity()) + "\tWeight=" + fmt.format(getWeight()) + "\tValue=" + fmt.format(getValue());
        /*
        // removed reporting for individual genes
        for (int i = 0; i < sackSize; i++) {
        str = str + contents[i] + "\t";
        str += (contents[i]) ? "1 " : "0 ";
        }
        */
        return str;
    }

    public int compareTo(Object other) {
        double baseCase = this.fitness;
        double otherBase = ((Knapsack) other).getFitness();
        if (baseCase == otherBase) {
            return 0;
        } else if (baseCase > otherBase) {
            return 1;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        // test function
        double values[] = {0.84, 0.11, 0.051, 0.01, 0.210, 0.75, 0.084, 0.23, 0.089, 0.14};
        double weights[] = {0.76, 0.52, 0.630, 0.42, 0.086, 0.77, 0.060, 0.54, 0.180, 0.57};
        int sacks = 10;
        int sackSize = 10;
        double capacity = 0.75;
        SimpleKnapsack[] array = new SimpleKnapsack[sacks];

        for (int i = 0; i < sacks; i++) {
            SimpleKnapsack sack = new SimpleKnapsack(sackSize, capacity, weights, values);
            System.out.println("Sack #" + i + ": " + sack);
            //System.out.println("Fitness: " + sack.fitnessFn() );
            array[i] = sack;
        }
        System.out.println("Mating Sack 0 with Sack 1: " + array[0].mate(array[1]));
        System.out.println("Mating Sack 0 with Sack 1: " + array[0].mate(array[1]));
        System.out.println("Mating Sack 0 with Sack 1: " + array[0].mate(array[1]));
        System.out.println("Mating Sack 0 with Sack 1: " + array[0].mate(array[1]));
    }


//    public Knapsack mateRandomLocusCrossover(Knapsack obj) {
//        // unused
//        boolean[] childContents = new boolean[sackSize];
//        int locus = genRandom.nextInt(sackSize);
//
//        if (locus <= sackSize - 1 && locus >= 0) {
//            // boolean newBit;
//
//            for (int i = 0; i < locus; i++)
//                childContents[i] = this.getObject(i);
//            for (int i = locus; i < sackSize; i++)
//                childContents[i] = obj.getObject(i);
//        } else {
//            System.err.println("locus = " + locus + " out of bounds, first parent returned as child.");
//            return this;
//        }
//
//        return new SimpleKnapsack(childContents, capacity, weights, values);
//    }

        /*

        // TODO try the following new crossover method:
        // instead of random for *each* gene, take chunks of genes from each parent
        // return mateRandomChunksCrossover(obj);

        // these crossovers will not work, the currentPopulationSize rapidly become identical
        // return mateUniformCrossover(obj, sackSize/2);
        // return mateStrongCrossover(obj);
        // return mateRandomLocusCrossover(obj);
        public Knapsack mateUniformCrossover(Knapsack obj, int locus) {
        // this will not work, the currentPopulationSize rapidly become identical
        boolean[] childContents = new boolean[sackSize];

        if (locus <= sackSize-1 && locus >= 0) {
        // boolean newBit;

        for (int i = 0; i < locus; i++)
        childContents[i] = this.getObject(i);
        for (int i = locus; i < sackSize; i++)
        childContents[i] = obj.getObject(i);
        } else {
        System.err.println("locus = " + locus + " out of bounds, first parent returned as child.");
        return this;
        }

        return  new SimpleKnapsack(childContents, capacity, weights, values);
        }

        public Knapsack mateStrongCrossover(Knapsack obj) {
        // this will not work, the currentPopulationSize rapidly become identical
        boolean[] childContents = new boolean[sackSize];
        double relativeStrengthParent1 = (GeneticDriver2.sacks.indexOf(this) * 1.0)/(GeneticDriver2.currentPopulationSize * 1.0);
        double relativeStrengthParent2 = (GeneticDriver2.sacks.indexOf(obj) * 1.0)/(GeneticDriver2.currentPopulationSize * 1.0);

        if (report < 3){
        // Debug
        System.out.println( "index this = " + GeneticDriver2.sacks.indexOf(this) + " index obj = " + GeneticDriver2.sacks.indexOf(obj));
        System.out.println( "total currentPopulationSize = " + GeneticDriver2.currentPopulationSize);
        }

        int tmp = (int)  (( (((Math.min(relativeStrengthParent1, relativeStrengthParent2) * 1.0)/ (Double) (relativeStrengthParent1 + relativeStrengthParent2))) * sackSize));

        int locus = (relativeStrengthParent1 < relativeStrengthParent2) ? tmp : sackSize - tmp - 1;

        if (locus <= sackSize-1 && locus >= 0) {
        // boolean newBit;

        for (int i = 0; i < locus; i++)
        childContents[i] = this.getObject(i);
        for (int i = locus; i < sackSize; i++)
        childContents[i] = obj.getObject(i);
        } else {
        System.err.println("locus = " + locus + " out of bounds, first parent returned as child.");
        return this;
        }

        return  new SimpleKnapsack(childContents, capacity, weights, values);
        }
        */
}
