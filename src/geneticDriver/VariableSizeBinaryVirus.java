package geneticDriver;

/* ************************************
Jonathan W. Lartigue
Auburn University
Masters Project
Spring 2002

(C) Copyright 2001-2002 by Jonathan W. Lartigue
All rights reserved.

File: VariableSizeBinaryVirus.java
************************************ */

import java.util.Random;


public class VariableSizeBinaryVirus extends SimpleBinaryVirus implements Virus {
	
    public VariableSizeBinaryVirus(int s, int sackSize)	{
        super(s,sackSize);
    }

    public void induction(boolean[] hostGenes) {
        if (infectionRate < 0)
            // Do nothing.
            return;

            //add to the viruses genotype by one gene

        //select the position at which to add a gene
        //(either at the beginning or the end of the string)
        int addLocus = ( rnd.nextBoolean() ) ? locus - 1 : locus + getSize();

        //check for valid locus
        if ( (addLocus >= hostGenes.length) || (addLocus < 0) )
            // the new locus is invalid, so exit
            return;

        //add to the genetic sub-string
        boolean[] newGenes = new boolean[genes.length + 1];
        if (addLocus < locus)
        {
            // System.out.println("*** DEBUG *** locus=" + locus + " addLocus=" + addLocus + "genes.l=" + genes.length + " newGenes.l=" + newGenes.length);
                newGenes[0] = hostGenes[addLocus];
            // System.out.println("*** DEBUG *** New gene for virus: " + ((hostGenes[addLocus])? "1" : "0"));
            for (int i = 1; i < genes.length; i++)
                newGenes[i] = genes[i];
            locus--;
        } else {
            // System.out.println("*** DEBUG *** locus=" + locus + " addLocus=" + addLocus + "genes.l=" + genes.length + " newGenes.l=" + newGenes.length);
            newGenes[newGenes.length-1] = hostGenes[addLocus];
            // System.out.println("*** DEBUG *** New gene for virus: " + ((hostGenes[addLocus]) ? "1" : "0"));
            for (int i = 0; i < genes.length - 1; i++)
                newGenes[i] = genes[i];
        }
        // System.out.println("*** DEBUG *** Updating virus's genetic sub-string");
        genes = newGenes;

    }

    // unused main function
    public static void main(String[] args)	{
        Random rnd = new Random();
        double values[] =	{ 0.84, 0.11, 0.051, 0.01, 0.210, 0.75, 0.084, 0.23, 0.089, 0.14 };
        double weights[] =	{ 0.76, 0.52, 0.630, 0.42, 0.086, 0.77, 0.060, 0.54, 0.180, 0.57 };
        double capacity = 5.75;  // hardcoded??
        SimpleKnapsack[] k = new SimpleKnapsack[20];
        VariableSizeBinaryVirus[] v = new VariableSizeBinaryVirus[20];


        for (int i = 0; i < k.length; i++) {
            // removed parameter passing random into constructor, knapsack has it's own random -edr
            k[i] = new SimpleKnapsack(values.length,capacity,weights, values);
            //System.out.println("Sack #" + i + ": " + k[i]);
        }
        for (int i = 0; i < v.length; i++) {
            v[i] = new VariableSizeBinaryVirus(2,values.length);
            // System.out.println("Virus #" + i + ": " + v[i]);
        }

        for(int i = 0; i < 20000; i++) {
            int k1 = rnd.nextInt(k.length);
            int k2 = rnd.nextInt(k.length);
            int virus = rnd.nextInt(v.length);
            SimpleKnapsack c = (SimpleKnapsack) k[k1].viralMate(k[k2],v[virus]);
            //System.out.println("Mating Sack 0 with Sack 1: " + c);
            double minval = 999999999;
            int min = -1;
            for (int j = 0; j < k.length; j++) {
                if (k[j].fitnessFn() < minval) {
                    minval = k[j].fitnessFn();
                    min = j;
                }
            }
            if (min != -1)
            k[min] = c;
        }
    }

}