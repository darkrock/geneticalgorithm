package geneticDriver;

/* ************************************
Jonathan W. Lartigue
Auburn University
Masters Project
Spring 2002
(C) Copyright 2001-2002 by Jonathan W. Lartigue
All rights reserved.
File: Virus.java
************************************ */
public interface Virus {
	
    public int getSize();
    public int getLocus();
    public boolean[] getGenes();
    public int getIR();
    public void incIR();
    public void decIR();
	
}
